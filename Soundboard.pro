QT       += core gui usb
QT       -= network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include(vendor/vendor.pri)


SOURCES += \
    Classes/rtaudioplayer.cpp \
	Libraries/RtAudio/RtAudio.cpp \
	Libraries/RtMidi/RtMidi.cpp \
    UI/buttonconfig.cpp \
    UI/moresettings.cpp \
    UI/soundboard.cpp \
    Widgets/hotkeybutton.cpp \
    Widgets/singlekeysequenceedit.cpp \
    main.cpp

HEADERS += \
    Classes/rtaudioplayer.h \
    Libraries/RtAudio/RtAudio.h \
	Libraries/RtMidi/RtMidi.h \
    Libraries/dr_wav.h \
    UI/buttonconfig.h \
    UI/moresettings.h \
    UI/soundboard.h \
    Widgets/hotkeybutton.h \
    Widgets/singlekeysequenceedit.h

FORMS += \
    UI/buttonconfig.ui \
    UI/moresettings.ui \
    UI/soundboard.ui

TRANSLATIONS += \
    Soundboard_en_GB.ts

RESOURCES += \
    res.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


win32 {
    RC_ICONS += applicationicon.ico

    DEFINES += __WINDOWS_WASAPI__
	DEFINES += __WINDOWS_MM__

    #LIBS += -lole32 -lwinmm -luuid -lksuser
	LIBS += -lole32 -lwinmm -lksuser -lmfplat -lmfuuid -lwmcodecdspuuid
}


QMAKE_TARGET_PRODUCT = "Soundboard"
QMAKE_TARGET_COMPANY = "Batty Bovine Productions, LLC"
QMAKE_TARGET_COPYRIGHT = "(c) 2020 Batty Bovine Productions, LLC. All Rights Reserved."
GENERATED_VERSION_NUMBER = $$system(perl versionup.pl -get)
VERSION = $${GENERATED_VERSION_NUMBER}
