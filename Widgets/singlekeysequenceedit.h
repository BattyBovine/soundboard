#ifndef SINGLEKEYSEQUENCEEDIT_H
#define SINGLEKEYSEQUENCEEDIT_H

#include <QKeySequenceEdit>

class SingleKeySequenceEdit : public QKeySequenceEdit
{
	Q_OBJECT

public:
	explicit SingleKeySequenceEdit(QWidget *parent = 0) : QKeySequenceEdit(parent){}
	~SingleKeySequenceEdit(){}

protected:
	virtual void keyPressEvent(QKeyEvent*) override;
};

#endif // SINGLEKEYSEQUENCEEDIT_H
