#ifndef SOUNDBOARD_H
#define SOUNDBOARD_H


#include <QMainWindow>
#include <QCloseEvent>
#include <QSystemTrayIcon>
#include <QSettings>
#include <QFileInfo>
#include <QUsb>

#include "Libraries/RtAudio/RtAudio.h"
#include "Libraries/RtMidi/RtMidi.h"


QT_BEGIN_NAMESPACE
namespace Ui { class Soundboard; }
QT_END_NAMESPACE

class Soundboard : public QMainWindow
{
	Q_OBJECT

public:
	Soundboard(QWidget *parent = nullptr);
	~Soundboard() override;

	static RtAudio::DeviceInfo findSelectedAudioDevice();

signals:
	void midiButtonPressed(QSize);

private slots:
	void saveSettings();
	void setEnabled(bool);
	void toggleEnabled();
	void setRunOnStartup(bool);
	void enableAllHotkeys();
	void reloadAllAudioDevices();
	void setAllVolumes(qreal);
	void disableAllHotkeys();
	void showFromTray(QSystemTrayIcon::ActivationReason);

	void getNewMidiDevice(QUsb::Id);
	void removedMidiDevice(QUsb::Id);
	void updateMidiState();
	void clearMidiState();
	void resetMidiState();

	void showMoreSettings();
	void applyMoreSettings();

	void executeAction(QSize);

	void exitApplication();

protected:
	virtual void changeEvent(QEvent*) override;
	virtual void closeEvent(QCloseEvent*) override;

	virtual void buildInterface();
	virtual void resizeWindow();
	virtual void clearInterface();

	virtual void loadSettings();
	virtual void loadMoreSettings();
	virtual void prepareTrayIcon();

private:
	Ui::Soundboard *ui;

	RtMidiIn *rtmiMidiInput = nullptr;
	qint8 iMidiPort = -1;
	friend void cMidiCallback(double, std::vector<unsigned char>*, void*);
	void midiCallback(double, std::vector<unsigned char>*);
	QUsb *usbListener = nullptr;

	class MoreSettings *msMoreSettingsPanel = nullptr;

	QSystemTrayIcon *stiTrayIcon = nullptr;
	QAction *actEnabled = nullptr;
	QAction *actRunOnStartup = nullptr;
	QAction *actStartMinimised = nullptr;
	QAction *actMinimiseToTaskbar = nullptr;
	QAction *actCloseToTaskbar = nullptr;
	QAction *actReconnect = nullptr;
	QSettings settingsGlobal;

	class QHotkey *hkEnable = nullptr;
	class QHotkey *hkReconnect = nullptr;
	QList<class HotkeyButton*> lHotkeyButtons;

	uint8_t bEnabled : 1;
	uint8_t bRunOnStartup : 1;
	uint8_t bStartMinimised : 1;
	uint8_t bMinimiseToTaskbar : 1;
	uint8_t bCloseToTaskbar : 1;

	QSize sizeButtonLayout = QSize(6,4);
	quint8	iButtonSize = 40;
};


#endif // SOUNDBOARD_H
