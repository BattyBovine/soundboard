#include "rtaudioplayer.h"

#include "UI/soundboard.h"

#define DR_WAV_IMPLEMENTATION
#include "Libraries/dr_wav.h"
#undef DR_WAV_IMPLEMENTATION


int cAudioCallback(void *outputbuffer, void *inputbuffer, unsigned int frames, double streamtime, RtAudioStreamStatus status, void *userdata)
{
	return static_cast<RtAudioPlayer*>(userdata)->audioCallback(outputbuffer, inputbuffer, frames, streamtime, status);
}


RtAudioPlayer::RtAudioPlayer(QObject *parent) : QObject(parent)
{
	this->bReady = false;

	this->rtaSoundPlayer = new RtAudio(RtAudio::WINDOWS_WASAPI);

	const quint16 numdevices = this->rtaSoundPlayer->getDeviceCount();
	quint16 i=0;
	for(i=0; i<numdevices; i++) {
		if(QString(this->rtaSoundPlayer->getDeviceInfo(i).name.c_str()) == this->rtdiSelectedDevice.name.c_str())
			break;
	}
	this->rtspStreamSettings.deviceId = (i>=numdevices) ? this->rtaSoundPlayer->getDefaultOutputDevice() : i;
	this->rtspStreamSettings.nChannels = 2;
	this->rtsoOptions.flags = RTAUDIO_MINIMIZE_LATENCY;

	this->timerStatusUpdate = new QTimer(this);
	QObject::connect(this->timerStatusUpdate, &QTimer::timeout, this, &RtAudioPlayer::updateAudioState);
}

RtAudioPlayer::~RtAudioPlayer()
{
	if(this->rtaSoundPlayer) {
		if(this->rtaSoundPlayer->isStreamRunning())
			this->rtaSoundPlayer->abortStream();
		if(this->rtaSoundPlayer->isStreamOpen())
			this->rtaSoundPlayer->closeStream();
		delete this->rtaSoundPlayer;
		this->rtaSoundPlayer = nullptr;
	}
	this->timerStatusUpdate->deleteLater();
}


void RtAudioPlayer::play()
{
	QMutexLocker lock(&this->mBufferMutex);
	if(this->bReady) {
		this->iSamplePosition = 0;
		if(!this->rtaSoundPlayer->isStreamRunning())
			this->rtaSoundPlayer->startStream();
		this->timerStatusUpdate->start(100);
		this->updateAudioState();
	}
}
void RtAudioPlayer::stop()
{
	QMutexLocker lock(&this->mBufferMutex);
	if(this->bReady) {
		if(this->rtaSoundPlayer->isStreamRunning())
			this->rtaSoundPlayer->stopStream();
		this->timerStatusUpdate->stop();
		this->updateAudioState();
	}
}
void RtAudioPlayer::togglePlayStop()
{
	if(this->rtaSoundPlayer->isStreamRunning())
		this->stop();
	else
		this->play();
}


int RtAudioPlayer::audioCallback(void *outputbuffer, void*, unsigned int frames, double/* streamtime*/, RtAudioStreamStatus)
{
	QMutexLocker lock(&this->mBufferMutex);

	float *outptr = static_cast<float*>(outputbuffer);

	for(quint32 frame=0; frame<frames; frame++) {
		if(this->iSamplePosition >= this->afiSoundInfo.totalSampleCount) {
			if(this->afiSoundInfo.loop)
				this->iSamplePosition = 0;
			else
				return 1;
		} else {
			quint64 sampleoffset = this->iSamplePosition * this->afiSoundInfo.bytesPerSample;
			for(unsigned int channel=0; channel < 2; channel++) {
				quint8 channeloffset = qMin<quint8>(this->afiSoundInfo.channels, channel);
				switch(this->afiSoundInfo.bytesPerSample) {
				case 4:
					if(this->afiSoundInfo.format == AudioFormat::IEEE_FLOAT)
						*outptr++ = *(reinterpret_cast<float*>(this->baPCMData.data() + sampleoffset) + channeloffset) * this->afiSoundInfo.amplitude;
					else
						*outptr++ = (*(reinterpret_cast<qint32*>(this->baPCMData.data() + sampleoffset) + channeloffset) * this->afiSoundInfo.amplitude) / INT32_MAX;
					break;
				case 2:
					*outptr++ = (*(reinterpret_cast<qint16*>(this->baPCMData.data() + sampleoffset) + channeloffset) * this->afiSoundInfo.amplitude) / INT16_MAX;
					break;
				default:
					*outptr++ = 0.0;
				}
				if(channel < this->afiSoundInfo.channels)
					this->iSamplePosition++;
			}
		}
	}

	return 0;
}


void RtAudioPlayer::reloadAudioOutput()
{
	if(this->rtaSoundPlayer->isStreamRunning())
		this->rtaSoundPlayer->abortStream();
	if(this->rtaSoundPlayer->isStreamOpen())
		this->rtaSoundPlayer->closeStream();

	const quint16 numdevices = this->rtaSoundPlayer->getDeviceCount();
	quint16 i=0;
	for(i=0; i<numdevices; i++) {
		if(QString(this->rtaSoundPlayer->getDeviceInfo(i).name.c_str()) == this->rtdiSelectedDevice.name.c_str())
			break;
	}
	this->rtspStreamSettings.deviceId = (i>=numdevices) ? this->rtaSoundPlayer->getDefaultOutputDevice() : i;
	this->rtspStreamSettings.nChannels = 2;

	RtAudio::StreamOptions options;
	options.flags = RTAUDIO_MINIMIZE_LATENCY;

	try {
		this->rtaSoundPlayer->openStream(&this->rtspStreamSettings, nullptr, RTAUDIO_FLOAT32, this->afiSoundInfo.sampleRate, &this->afiSoundInfo.bufferSize, &cAudioCallback, this, &options);
	} catch (RtAudioError &e) {
		std::cerr << e.getMessage();
	}
}

void RtAudioPlayer::loadSoundFile()
{
	if(this->rtaSoundPlayer) {
		if(this->rtaSoundPlayer->isStreamRunning())
			this->rtaSoundPlayer->abortStream();
		if(this->rtaSoundPlayer->isStreamOpen())
			this->rtaSoundPlayer->closeStream();
	}

	this->bReady = false;

	drwav wav;
	if(drwav_init_file(&wav, this->fiSoundFile.absoluteFilePath().toStdString().c_str())) {
		this->afiSoundInfo.format = (wav.fmt.formatTag==DR_WAVE_FORMAT_IEEE_FLOAT) ? AudioFormat::IEEE_FLOAT : AudioFormat::PCM;
		this->afiSoundInfo.sampleRate = wav.sampleRate;
		this->afiSoundInfo.channels = wav.channels;
		this->afiSoundInfo.bitsPerSample = wav.bitsPerSample;
		this->afiSoundInfo.bytesPerSample = wav.bytesPerSample;
		this->afiSoundInfo.totalSampleCount = wav.totalSampleCount;

		this->rtdiSelectedDevice = Soundboard::findSelectedAudioDevice();

		std::vector<unsigned int> supportedsamplerates = this->rtdiSelectedDevice.sampleRates;
//		RtAudioFormat supportedformats = this->rtdiSelectedDevice.nativeFormats;
		bool ratesupported = false;
		for(const quint32 supportedrate : supportedsamplerates) {
			if(supportedrate == this->afiSoundInfo.sampleRate) {
				ratesupported = true;
				break;
			}
		}

		if(ratesupported) {
			char *pcmbuffer = new char[this->afiSoundInfo.totalSampleCount*this->afiSoundInfo.bytesPerSample*this->afiSoundInfo.channels];
			quint64 bytecount = drwav_read_raw(&wav, this->afiSoundInfo.totalSampleCount*this->afiSoundInfo.bytesPerSample, pcmbuffer);
			this->baPCMData.clear();
			this->baPCMData.append(pcmbuffer, bytecount);
			delete[] pcmbuffer;

			this->bReady = true;

			this->reloadAudioOutput();
		}/* else {
			qDebug() << "Audio format not supported.";
		}*/
		drwav_uninit(&wav);
	}

	this->updateAudioState();
}


void RtAudioPlayer::updateAudioState()
{
	PlaybackState newstate;
	if(!this->fiSoundFile.isFile())
		newstate = PlaybackState::Error;
	else if(!this->rtaSoundPlayer->isStreamOpen())
		newstate = PlaybackState::Disabled;
	else if(this->rtaSoundPlayer->isStreamRunning())
		newstate = PlaybackState::Playing;
	else
		newstate = PlaybackState::Idle;

	this->psState = newstate;
	emit(this->stateUpdated(newstate));
}
