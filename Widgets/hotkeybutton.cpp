#include "hotkeybutton.h"

#include <QSettings>
#include <QBuffer>
#include <QApplication>
#include <QDrag>
#include <QMimeData>

#include "UI/soundboard.h"


HotkeyButton::HotkeyButton(QPoint pos, QWidget *parent) : QPushButton(parent)
{
	this->pPosition = pos;

	this->hkHotkey = new QHotkey(this);
	QObject::connect(this->hkHotkey, &QHotkey::activated, this, &HotkeyButton::executeAction);

	QString category = ButtonConfig::buttonConfigCategory(this->pPosition);
	this->bLocalEnabled = this->settings.value(category+"Enabled", false).toBool();
	this->bStartStop = this->settings.value(category+"StartStop", false).toBool();
	this->bLoop = this->settings.value(category+"Loop", false).toBool();

	if(this->bLocalEnabled) {
		this->setKeySequence(QKeySequence(this->settings.value(category+"KeySequence","").toString()));
		this->fiSoundFile = QFileInfo(this->settings.value(category+"SoundFile","").toString());
		this->rtapAudioPlayer = new RtAudioPlayer(this);
		QObject::connect(this->rtapAudioPlayer, &RtAudioPlayer::stateUpdated, this, &HotkeyButton::changeState);
		this->rtapAudioPlayer->setSoundFile(this->fiSoundFile);
		this->rtapAudioPlayer->setVolume(this->settings.value("OutputVolume").toFloat());
		this->rtapAudioPlayer->setLoop(this->bLoop);
	} else {
		this->changeState(PlaybackState::Disabled);
	}

	if(this->rtapAudioPlayer)
		this->changeState(this->rtapAudioPlayer->playbackState());

	this->setAcceptDrops(true);
}
HotkeyButton::~HotkeyButton()
{
	if(this->rtapAudioPlayer) delete this->rtapAudioPlayer;
	this->hkHotkey->deleteLater();
}


void HotkeyButton::mousePressEvent(QMouseEvent *e)
{
	switch(e->button()) {
	case Qt::LeftButton:
		this->pDragStart = e->pos();
		e->accept();
		break;

	case Qt::RightButton:
		this->edit();
		emit(this->beginButtonUpdate());
		e->accept();
		break;

	case Qt::MiddleButton:
		this->setLocalEnabled(!this->bLocalEnabled);
		e->accept();
		break;

	default:
		QPushButton::mousePressEvent(e);
	}
}
void HotkeyButton::mouseMoveEvent(QMouseEvent *e)
{
	if(!(e->buttons() & Qt::LeftButton))	return;
	if((e->pos() - this->pDragStart).manhattanLength() < QApplication::startDragDistance())	return;

	QDrag *drag = new QDrag(this);

	QMimeData *mimedata = new QMimeData();
	QString buttondata = QUrl::toPercentEncoding(this->fiSoundFile.absoluteFilePath())
			.append(":")
			.append(this->bLoop?"t":"f")
			.append(":")
			.append(this->bStartStop?"t":"f")
			.append(":")
			.append(this->bLocalEnabled?"t":"f");
	mimedata->setData("text/plain", buttondata.toUtf8());
	drag->setMimeData(mimedata);
	//drag->setPixmap();

//	Qt::DropAction dropaction = drag->exec(Qt::CopyAction | Qt::MoveAction);
}
void HotkeyButton::mouseReleaseEvent(QMouseEvent *e)
{
	this->pDragStart = QPoint(INT16_MIN,INT16_MIN);
	switch(e->button()) {
	case Qt::LeftButton:
		this->executeAction();
		e->accept();
		break;
	default:
		e->ignore();
	}
}

void HotkeyButton::dragEnterEvent(QDragEnterEvent *e)
{
	if(e->mimeData()->hasFormat("text/plain"))
		e->acceptProposedAction();
}
void HotkeyButton::dropEvent(QDropEvent *e)
{
	QStringList data = QString(e->mimeData()->data("text/plain")).split(":");
	this->fiSoundFile = QFileInfo(QUrl::fromPercentEncoding(data[0].toUtf8()));
	if(data.size()>1)
		this->bLoop = data[1].startsWith("t");
	if(data.size()>2)
		this->bStartStop = data[2].startsWith("t");
	if(data.size()>3)
		this->setLocalEnabled(data[3].startsWith("t"));
	this->saveSettings();
	e->acceptProposedAction();
}


void HotkeyButton::setLocalEnabled(bool e)
{
	this->bLocalEnabled=e;

	if(this->bLocalEnabled) {
		if(!this->rtapAudioPlayer) {
			this->rtapAudioPlayer = new RtAudioPlayer(this);
			QObject::connect(this->rtapAudioPlayer, &RtAudioPlayer::stateUpdated, this, &HotkeyButton::changeState);
		}
		this->rtapAudioPlayer->setSoundFile(this->fiSoundFile);
		this->rtapAudioPlayer->setVolume(this->settings.value("OutputVolume").toFloat());
		this->rtapAudioPlayer->setLoop(this->bLoop);
		this->registerHotkey();
	} else {
		this->hkHotkey->setRegistered(false);
		if(this->rtapAudioPlayer) {
			QObject::disconnect(this->rtapAudioPlayer, &RtAudioPlayer::stateUpdated, this, &HotkeyButton::changeState);
			this->rtapAudioPlayer->stop();
			delete this->rtapAudioPlayer;
			this->rtapAudioPlayer = nullptr;
		}
		this->changeState(PlaybackState::Disabled);
	}

	QString category = ButtonConfig::buttonConfigCategory(this->pPosition);
	this->settings.setValue(category+"Enabled", this->bLocalEnabled);
}


void HotkeyButton::edit()
{
	this->widgetConfigWindow = new ButtonConfig(this->pPosition);
	QObject::connect(widgetConfigWindow, &ButtonConfig::rejected, this, &HotkeyButton::getNewConfiguration);
	widgetConfigWindow->show();
}

void HotkeyButton::getNewConfiguration()
{
	bool enabled = this->widgetConfigWindow->enabled();
	this->bStartStop = this->widgetConfigWindow->startStop();
	this->ksShortcut = this->widgetConfigWindow->inputKey();
	this->fiSoundFile = this->widgetConfigWindow->soundFile();
	this->bLoop = this->widgetConfigWindow->loop();

	this->widgetConfigWindow->deleteLater();
	this->widgetConfigWindow = nullptr;

	this->setLocalEnabled(enabled);

	this->saveSettings();

	emit(this->buttonUpdated());
}
void HotkeyButton::saveSettings()
{
	QString category = ButtonConfig::buttonConfigCategory(this->pPosition);
	this->settings.setValue(category+"StartStop", this->bStartStop);
	this->settings.setValue(category+"KeySequence", this->ksShortcut.toString());
	this->settings.setValue(category+"SoundFile", this->fiSoundFile.absoluteFilePath());
	this->settings.setValue(category+"Loop", this->bLoop);
}

void HotkeyButton::registerHotkey()
{
	this->hkHotkey->setShortcut(this->ksShortcut, this->bGlobalEnabled);
}

void HotkeyButton::executeAction()
{
	if(this->bLocalEnabled && this->bGlobalEnabled) {
		if(this->bStartStop)
			this->rtapAudioPlayer->togglePlayStop();
		else
			this->rtapAudioPlayer->play();
	}
}


void HotkeyButton::setNewAudioDevice(RtAudio::DeviceInfo rtdi)
{
	if(this->rtapAudioPlayer)
		this->rtapAudioPlayer->setAudioDevice(rtdi);
}

void HotkeyButton::changeVolume(qreal db)
{
	if(this->rtapAudioPlayer)
		this->rtapAudioPlayer->setVolume(db);
}

void HotkeyButton::changeState(PlaybackState ps)
{
	switch(ps) {
	case PlaybackState::Disabled:
		this->setStyleSheet("background-color:red");
		break;
	case PlaybackState::Error:
		this->setStyleSheet("background-color:orange");
		break;
	case PlaybackState::Playing:
		this->setStyleSheet("background-color:green");
		break;
	case PlaybackState::Idle:
		this->setStyleSheet("");
		break;
	}
}
