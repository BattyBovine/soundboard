#include "buttonconfig.h"
#include "ui_buttonconfig.h"

#include <QFileDialog>

#include "Libraries/dr_wav.h"


ButtonConfig::ButtonConfig(QPoint position, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ButtonConfig)
{
	ui->setupUi(this);

	this->pPosition = position;

	QString category = ButtonConfig::buttonConfigCategory(this->pPosition);
	this->enableHotkey(this->settings.value(category+"Enabled", ui->checkEnabled->isChecked()).toBool());
	this->setStartStop(this->settings.value(category+"StartStop", ui->checkStartStop->isChecked()).toBool());
	this->setKeySequence(QKeySequence(this->settings.value(category+"KeySequence","").toString()));
	this->setSoundFile(this->settings.value(category+"SoundFile","").toString());
	this->setLoop(this->settings.value(category+"Loop", ui->checkLoop->isChecked()).toBool());

	QObject::connect(ui->checkEnabled, &QCheckBox::toggled, this, &ButtonConfig::enableHotkey);
	QObject::connect(ui->checkStartStop, &QCheckBox::toggled, this, &ButtonConfig::setStartStop);
	QObject::connect(ui->checkLoop, &QCheckBox::toggled, this, &ButtonConfig::setLoop);

	ui->checkLoop->setEnabled(this->bStartStop);
}

ButtonConfig::~ButtonConfig()
{
	delete ui;
}


void ButtonConfig::enableHotkey(bool h)
{
	this->bEnabled = h;
	ui->checkEnabled->setChecked(h);
}

void ButtonConfig::setStartStop(bool s)
{
	this->bStartStop = s;
	ui->checkStartStop->setChecked(s);
	ui->checkLoop->setEnabled(s);
}

void ButtonConfig::setKeySequence(QKeySequence ks)
{
	this->ksShortcut = ks;
	ui->kseInputKey->setKeySequence(ks);
}

void ButtonConfig::setSoundFile(QFileInfo fi)
{
	this->fiSound = fi;
	if(!fi.isReadable()) {
		ui->buttonSound->setText(tr("None"));
		return;
	}
	ui->buttonSound->setText(fi.baseName());
}

void ButtonConfig::loadSoundFile()
{
	QFileInfo fileinfo(QFileDialog::getOpenFileName(this, tr("Open Sound File"), "", tr("Sound Files (*.wav)")));
	this->setSoundFile(fileinfo);
}

void ButtonConfig::setLoop(bool loop)
{
	this->bLoop = loop;
	ui->checkLoop->setChecked(loop);
}
