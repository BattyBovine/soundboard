#include "moresettings.h"
#include "ui_moresettings.h"

#include "Libraries/RtAudio/RtAudio.h"


MoreSettings::MoreSettings(QSize buttonlayout, quint8 buttonsize, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::MoreSettings)
{
	ui->setupUi(this);

	ui->hsColumns->setValue(this->settingsGlobal.value("ButtonColumns", buttonlayout.width()).toUInt());
	ui->labelColumnsCount->setText(QString::number(ui->hsColumns->value()));
	QObject::connect(ui->hsColumns, &QSlider::valueChanged, this, &MoreSettings::changeColumnsValue);

	ui->hsRows->setValue(this->settingsGlobal.value("ButtonRows", buttonlayout.height()).toUInt());
	ui->labelRowsCount->setText(QString::number(ui->hsRows->value()));
	QObject::connect(ui->hsRows, &QSlider::valueChanged, this, &MoreSettings::changeRowsValue);

	ui->hsButtonSize->setValue(this->settingsGlobal.value("ButtonSize", buttonsize).toUInt());
	ui->labelButtonSizeValue->setText(QString::number(ui->hsButtonSize->value()));
	QObject::connect(ui->hsButtonSize, &QSlider::valueChanged, this, &MoreSettings::changeButtonSizeValue);

	ui->kseEnableToggle->setKeySequence(QKeySequence(this->settingsGlobal.value("EnableHotkey", "").toString()));
	QObject::connect(ui->kseEnableToggle, &QKeySequenceEdit::keySequenceChanged, this, &MoreSettings::changeEnableToggleShortcut);

	ui->kseReconnect->setKeySequence(QKeySequence(this->settingsGlobal.value("ReconnectHotkey", "").toString()));
	QObject::connect(ui->kseReconnect, &QKeySequenceEdit::keySequenceChanged, this, &MoreSettings::changeReconnectShortcut);

	RtAudio rtaudio(RtAudio::WINDOWS_WASAPI);
	QStringList devicenames;
	const quint8 numdevices = rtaudio.getDeviceCount();
	for(quint8 i=0; i<numdevices; i++) {
		const RtAudio::DeviceInfo info = rtaudio.getDeviceInfo(i);
		if(info.outputChannels <= 0)
			continue;
		const QString devicename = info.name.c_str();
		if(!devicenames.contains(devicename)) {
			ui->comboOutputDevice->addItem(devicename);
			devicenames.append(devicename);
		}
	}
	QString currentdevice = this->settingsGlobal.value("OutputDevice").toString();
	if(currentdevice.isEmpty() || !devicenames.contains(currentdevice))
		ui->comboOutputDevice->setCurrentText(rtaudio.getDeviceInfo(rtaudio.getDefaultOutputDevice()).name.c_str());
	else
		ui->comboOutputDevice->setCurrentText(currentdevice);
	QObject::connect(ui->comboOutputDevice, &QComboBox::currentTextChanged, this, &MoreSettings::changeOutputDevice);

//	QList<QAudioDeviceInfo> devices = QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
//	QStringList devicenames, uniquedevicenames;
//	for(const QAudioDeviceInfo &device : devices) {
//		const QString devicename = device.deviceName();
//		if(!devicenames.contains(devicename)) {
//			const QRegularExpressionMatch uniquedevicematch(QRegularExpression("\\((.*)\\)$").match(devicename));
//			if(uniquedevicematch.hasMatch()) {
//				const QString uniquedevicename = uniquedevicematch.captured(1);
//				if(!uniquedevicenames.contains(uniquedevicename)) {
//					ui->comboOutputDevice->addItem(devicename);
//					devicenames.append(devicename);
//					uniquedevicenames.append(uniquedevicename);
//				}
//			}
//		}
//	}
//	QString currentdevice = this->settingsGlobal.value("OutputDevice").toString();
//	if(currentdevice.isEmpty() || !devicenames.contains(currentdevice))
//		ui->comboOutputDevice->setCurrentText(QAudioDeviceInfo::defaultOutputDevice().deviceName());
//	else
//		ui->comboOutputDevice->setCurrentText(currentdevice);
//	QObject::connect(ui->comboOutputDevice, &QComboBox::currentTextChanged, this, &MoreSettings::changeOutputDevice);

	qreal db = this->settingsGlobal.value("OutputVolume", 0).toFloat();
	ui->hsVolume->setValue(db*10.0);
	this->setVolumeText(db);
	QObject::connect(ui->hsVolume, &QSlider::valueChanged, this, &MoreSettings::changeVolume);
}

MoreSettings::~MoreSettings()
{
	delete ui;
}


void MoreSettings::changeColumnsValue(quint8 c)
{
	ui->labelColumnsCount->setText(QString::number(c));
	this->settingsGlobal.setValue("ButtonColumns", c);
}

void MoreSettings::changeRowsValue(quint8 r)
{
	ui->labelRowsCount->setText(QString::number(r));
	this->settingsGlobal.setValue("ButtonRows", r);
}

void MoreSettings::changeButtonSizeValue(quint8 s)
{
	ui->labelButtonSizeValue->setText(QString::number(s));
	this->settingsGlobal.setValue("ButtonSize", s);
}

void MoreSettings::changeEnableToggleShortcut(QKeySequence k)
{
	this->settingsGlobal.setValue("EnableHotkey", k.toString());
}

void MoreSettings::changeReconnectShortcut(QKeySequence k)
{
	this->settingsGlobal.setValue("ReconnectHotkey", k.toString());
}


void MoreSettings::changeOutputDevice(QString device)
{
	this->settingsGlobal.setValue("OutputDevice", device);
	emit(this->audioDeviceUpdated(device));
}

void MoreSettings::changeVolume(qint16 db)
{
	qreal dbfloat = db/10.0;
	this->settingsGlobal.setValue("OutputVolume", dbfloat);
	this->setVolumeText(dbfloat);
	emit(this->audioVolumeUpdated(dbfloat));
}


void MoreSettings::setVolumeText(qreal db)
{
	ui->labelVolumeValue->setText((db <= (ui->hsVolume->minimum()/10)) ? QString("-200dB") : QString("%1dB").arg(QString::number(db,'f',1)));
}


void MoreSettings::closeEvent(QCloseEvent *e)
{
	this->hide();
	e->ignore();
	emit(this->closedMoreSettings());
}
