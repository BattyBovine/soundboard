#ifndef RTAUDIOPLAYER_H
#define RTAUDIOPLAYER_H

#include <QObject>
#include <QMutex>
#include <QFileInfo>
#include <QTimer>

#include <cmath>

#include "Libraries/RtAudio/RtAudio.h"


enum class AudioFormat : quint8 {
	PCM,
	IEEE_FLOAT
};

enum class PlaybackState : quint8 {
	Disabled,
	Idle,
	Playing,
	Error
};

struct AudioFileInfo {
	AudioFormat format = AudioFormat::PCM;
	RtAudioFormat rtformat = RTAUDIO_SINT16;
	quint32 sampleRate = 44100;
	quint8 channels = 2;
	quint8 bitsPerSample = 16;
	quint8 bytesPerSample = 2;
	quint64 totalSampleCount = 0;
	unsigned int bufferSize = 32;
	qreal amplitude = 0.1;
	bool loop = false;
};

class RtAudioPlayer : public QObject
{
	Q_OBJECT

public:
	RtAudioPlayer(QObject *parent=nullptr);
	~RtAudioPlayer();

	void play();
	void stop();
	void togglePlayStop();

	void setSoundFile(QFileInfo f) { this->fiSoundFile=f; this->loadSoundFile(); }
	void setAudioDevice(RtAudio::DeviceInfo i) { this->rtdiSelectedDevice=i; this->reloadAudioOutput(); }
	void setVolume(qreal db) { this->afiSoundInfo.amplitude = db <= -60.0 ? 0.0 : RtAudioPlayer::dBToAmplitude(db); }
	void setLoop(bool l) { this->afiSoundInfo.loop=l; }

	AudioFormat format() { return this->afiSoundInfo.format; }
	RtAudioFormat rtformat() { return this->afiSoundInfo.rtformat; }
	quint32 sampleRate() { return this->afiSoundInfo.sampleRate; }
	quint8 channels() { return this->afiSoundInfo.channels; }
	quint8 bitsPerSample() { return this->afiSoundInfo.bitsPerSample; }
	quint8 bytesPerSample() { return this->afiSoundInfo.bytesPerSample; }
	quint64 totalSampleCount() { return this->afiSoundInfo.totalSampleCount; }
	unsigned int bufferSize() { return this->afiSoundInfo.bufferSize; }
	qreal amplitude() { return this->afiSoundInfo.amplitude; }

	QFileInfo soundFile() { return this->fiSoundFile; }
	PlaybackState playbackState() { return this->psState; }

	static inline float amplitudeTodB(qreal a) { return 20.0 * log10(a); }
	static inline float dBToAmplitude(qreal d) { return pow(10.0, d/20.0); }

signals:
	void stateUpdated(PlaybackState);

private slots:
	void updateAudioState();

private:
	void reloadAudioOutput();
	void loadSoundFile();

	friend int cAudioCallback(void*, void*, unsigned int, double, RtAudioStreamStatus, void*);
	int audioCallback(void*, void*, unsigned int, double, RtAudioStreamStatus);

	RtAudio *rtaSoundPlayer = nullptr;
	QFileInfo fiSoundFile;
	QByteArray baPCMData;
	quint64 iSamplePosition = 0;

	RtAudio::DeviceInfo rtdiSelectedDevice;
	RtAudio::StreamParameters rtspStreamSettings;
	RtAudio::StreamOptions rtsoOptions;
	AudioFileInfo afiSoundInfo;

	QMutex mBufferMutex;
	PlaybackState psState = PlaybackState::Disabled;
	QTimer *timerStatusUpdate = nullptr;

	quint8 bReady : 1;
};

#endif // RTAUDIOPLAYER_H
