#ifndef HOTKEYBUTTON_H
#define HOTKEYBUTTON_H

#include <QPushButton>
#include <QHotkey>
#include <QMutex>

#include <cmath>

#include "Classes/rtaudioplayer.h"
#include "UI/buttonconfig.h"


class HotkeyButton : public QPushButton
{
	Q_OBJECT

public:
	explicit HotkeyButton(QPoint, QWidget *parent = nullptr);
	~HotkeyButton();

	static inline float amplitudeTodB(qreal a) { return 20.0 * log10(a); }
	static inline float dBToAmplitude(qreal d) { return pow(10.0, d/20.0); }

signals:
	void beginButtonUpdate();
	void buttonUpdated();

public slots:
	void executeAction();

	void setLocalEnabled(bool e);
	void setGlobalEnabled(bool e) {
		this->bGlobalEnabled=e;
		this->hkHotkey->setRegistered(e);
	}
	void setStartStop(bool s) { this->bStartStop=s; }
	void setKeySequence(QKeySequence ks) { this->ksShortcut=ks; this->registerHotkey(); }

	void setNewAudioDevice(RtAudio::DeviceInfo);
	void changeVolume(qreal);

protected slots:
	virtual void mousePressEvent(QMouseEvent*) override;
	virtual void mouseMoveEvent(QMouseEvent*) override;
	virtual void mouseReleaseEvent(QMouseEvent*) override;

	virtual void dragEnterEvent(QDragEnterEvent*) override;
	virtual void dropEvent(QDropEvent*) override;

private slots:
	void edit();
	void getNewConfiguration();
	void saveSettings();
	void changeState(PlaybackState);

private:
	void registerHotkey();

	friend int cAudioCallback(void*, void*, unsigned int, double, RtAudioStreamStatus, void*);
	int audioCallback(void*, void*, unsigned int, double, RtAudioStreamStatus);

	ButtonConfig *widgetConfigWindow = nullptr;
	QPoint pPosition;

	QSettings settings;
	QHotkey *hkHotkey = nullptr;
	QKeySequence ksShortcut;

	RtAudioPlayer *rtapAudioPlayer = nullptr;
	QMutex mSampleLock;
	QFileInfo fiSoundFile;

	QPoint pDragStart;

	quint8 bLocalEnabled : 1;
	quint8 bGlobalEnabled : 1;
	quint8 bStartStop : 1;
	quint8 bLoop : 1;
};


#endif // HOTKEYBUTTON_H
