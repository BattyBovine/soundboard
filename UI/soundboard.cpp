#include "soundboard.h"
#include "ui_soundboard.h"

#include <QMenu>
#include <QDir>
#include <QSettings>
#include <QHotkey>
#include <QStyle>
#include <QScreen>

#include "Widgets/hotkeybutton.h"
#include "UI/moresettings.h"


void cMidiCallback(double deltatime, std::vector<unsigned char> *message, void *userdata)
{
	return static_cast<Soundboard*>(userdata)->midiCallback(deltatime, message);
}

Soundboard::Soundboard(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::Soundboard)
{
	ui->setupUi(this);

	this->hkEnable = new QHotkey(QKeySequence(this->settingsGlobal.value("EnableHotkey","").toString()), true, this);
	QObject::connect(this->hkEnable, &QHotkey::activated, this, &Soundboard::toggleEnabled);

	this->hkReconnect = new QHotkey(QKeySequence(this->settingsGlobal.value("ReconnectHotkey","").toString()), true, this);
	QObject::connect(this->hkReconnect, &QHotkey::activated, this, &Soundboard::resetMidiState);

	this->loadSettings();
	this->loadMoreSettings();
	this->buildInterface();

	if(this->bStartMinimised) {
		if(!this->bMinimiseToTaskbar)
			this->showMinimized();
	} else {
		this->showNormal();
	}

	try {
		this->rtmiMidiInput = new RtMidiIn();
	} catch (RtMidiError &e) {
		qDebug() << e.getMessage().c_str();
	}
	this->updateMidiState();
	QObject::connect(this, &Soundboard::midiButtonPressed, this, &Soundboard::executeAction);

	this->usbListener = new QUsb(this);
	QObject::connect(this->usbListener, &QUsb::deviceInserted, this, &Soundboard::getNewMidiDevice);
	QObject::connect(this->usbListener, &QUsb::deviceRemoved, this, &Soundboard::removedMidiDevice);

	this->prepareTrayIcon();
}

Soundboard::~Soundboard()
{
	this->rtmiMidiInput->cancelCallback();
	this->rtmiMidiInput->closePort();
	delete this->rtmiMidiInput;

	delete ui;
}


void Soundboard::midiCallback(double, std::vector<unsigned char> *message)
{
	// Read note on and off messages, interpreting the note and velocity as the column and row of the pressed button
	const quint8 status = message->at(0);
	if((status&0xF0)==0x90) {
		if((status&0x0F)==0x00) {
			emit(this->midiButtonPressed(QSize(message->at(2),message->at(1))));
		}
	}
}
void Soundboard::executeAction(QSize pos)
{
	const quint16 buttonindex = (this->sizeButtonLayout.width()*pos.height()) + pos.width();
	if(buttonindex < this->lHotkeyButtons.size())
		this->lHotkeyButtons[buttonindex]->executeAction();
}

void Soundboard::getNewMidiDevice(QUsb::Id id)
{
	if(id.vid == 0x16C0) {
		if(id.pid >= 0x0484 && id.pid <= 0x0488) {
			if(!this->rtmiMidiInput->isPortOpen()) {
				this->updateMidiState();
			}
		}
	}
}
void Soundboard::removedMidiDevice(QUsb::Id id)
{
	if(id.vid == 0x16C0) {
		if(id.pid >= 0x0484 && id.pid <= 0x0488) {
			if(this->rtmiMidiInput->isPortOpen()) {
				this->clearMidiState();
			}
		}
	}
}
void Soundboard::updateMidiState()
{
	quint16 portcount = this->rtmiMidiInput->getPortCount();
	QString portname;
	for(quint16 i=0; i<portcount; i++) {
		try {
			portname = this->rtmiMidiInput->getPortName(i).c_str();
		} catch (RtMidiError &e) {
			qDebug() << e.getMessage().c_str();
		}
		if(portname.contains("Soundboard")) {
			if(this->rtmiMidiInput->isPortOpen()) {
				if(this->iMidiPort != i) {
					this->rtmiMidiInput->cancelCallback();
					this->rtmiMidiInput->closePort();
					this->iMidiPort = i;
					this->rtmiMidiInput->openPort(this->iMidiPort, "MIDI Soundboard Input");
					this->rtmiMidiInput->setCallback(&cMidiCallback, this);
				}
			} else {
				this->iMidiPort = i;
				this->rtmiMidiInput->openPort(this->iMidiPort, "MIDI Soundboard Input");
				this->rtmiMidiInput->setCallback(&cMidiCallback, this);
			}
			return;
		}
	}

	// If no appropriate device could be found, cancel everything and move on
	this->clearMidiState();
}
void Soundboard::clearMidiState()
{
	if(this->rtmiMidiInput->isPortOpen()) {
		this->rtmiMidiInput->cancelCallback();
		this->iMidiPort = -1;
	}
	this->rtmiMidiInput->closePort();
}
void Soundboard::resetMidiState()
{
	this->clearMidiState();
	this->updateMidiState();
}


void Soundboard::prepareTrayIcon()
{
	this->stiTrayIcon = new QSystemTrayIcon(QIcon(":/icons/applicationicon.ico"));
	QMenu *traymenu = new QMenu(tr("Preferences"));

	// Enable hotkeys
	this->actEnabled = new QAction();
	this->actEnabled->setText(tr("Enable"));
	this->actEnabled->setCheckable(true);
	this->actEnabled->setChecked(this->bEnabled);
	QObject::connect(this->actEnabled, &QAction::toggled, this, &Soundboard::setEnabled);
	traymenu->addAction(this->actEnabled);

	traymenu->addSeparator();

#ifdef Q_OS_WINDOWS
	// Run on startup
	this->actRunOnStartup = new QAction();
	this->actRunOnStartup->setText(tr("Run on startup"));
	this->actRunOnStartup->setCheckable(true);
	this->actRunOnStartup->setChecked(this->bRunOnStartup);
	QObject::connect(this->actRunOnStartup, &QAction::toggled, this, &Soundboard::setRunOnStartup);
	traymenu->addAction(this->actRunOnStartup);
#endif

	// Start minimised toggle setup
	this->actStartMinimised = new QAction();
	this->actStartMinimised->setText(tr("Start Minimised"));
	this->actStartMinimised->setCheckable(true);
	this->actStartMinimised->setChecked(this->bStartMinimised);
	QObject::connect(this->actStartMinimised, &QAction::toggled, this, &Soundboard::saveSettings);
	traymenu->addAction(this->actStartMinimised);

	// Minimise to taskbar toggle setup
	this->actMinimiseToTaskbar = new QAction();
	this->actMinimiseToTaskbar->setText(tr("Minimise To Taskbar"));
	this->actMinimiseToTaskbar->setCheckable(true);
	this->actMinimiseToTaskbar->setChecked(this->bMinimiseToTaskbar);
	QObject::connect(this->actMinimiseToTaskbar, &QAction::toggled, this, &Soundboard::saveSettings);
	traymenu->addAction(this->actMinimiseToTaskbar);

	// Close to taskbar toggle setup
	this->actCloseToTaskbar = new QAction();
	this->actCloseToTaskbar->setText(tr("Close To Taskbar"));
	this->actCloseToTaskbar->setCheckable(true);
	this->actCloseToTaskbar->setChecked(this->bCloseToTaskbar);
	QObject::connect(this->actCloseToTaskbar, &QAction::toggled, this, &Soundboard::saveSettings);
	traymenu->addAction(this->actCloseToTaskbar);

	traymenu->addSeparator();

	// Reconnect to MIDI device
	this->actReconnect = new QAction();
	this->actReconnect->setText(tr("Reconnect"));
	QObject::connect(this->actReconnect, &QAction::triggered, this, &Soundboard::resetMidiState);
	traymenu->addAction(this->actReconnect);

	traymenu->addSeparator();

	traymenu->addAction(tr("More settings..."), this, &Soundboard::showMoreSettings);

	traymenu->addSeparator();
	traymenu->addAction(tr("Quit"), this, &Soundboard::exitApplication);
	this->stiTrayIcon->setContextMenu(traymenu);
	this->stiTrayIcon->setToolTip(QApplication::applicationName());
	this->stiTrayIcon->show();

	QObject::connect(this->stiTrayIcon, &QSystemTrayIcon::activated, this, &Soundboard::showFromTray);
}


void Soundboard::buildInterface()
{
	for(uint8_t y=0; y<this->sizeButtonLayout.height(); y++) {
		for(uint8_t x=0; x<this->sizeButtonLayout.width(); x++) {
			QPoint pos(x,y);
			HotkeyButton *hkb = new HotkeyButton(pos);
			hkb->setGlobalEnabled(this->bEnabled);
			hkb->setMinimumSize(QSize(1,1));
			hkb->setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum));
			QObject::connect(hkb, &HotkeyButton::beginButtonUpdate, this, &Soundboard::disableAllHotkeys);
			QObject::connect(hkb, &HotkeyButton::buttonUpdated, this, &Soundboard::enableAllHotkeys);
			ui->gridButtons->addWidget(hkb,y,x);
			this->lHotkeyButtons.append(hkb);
		}
	}

	this->resizeWindow();
}
void Soundboard::resizeWindow()
{
	QSize windowsize(
		(this->sizeButtonLayout.width() * (this->iButtonSize+ui->gridButtons->horizontalSpacing())) + (this->contentsMargins().left()+this->contentsMargins().right()),
		(this->sizeButtonLayout.height() * (this->iButtonSize+ui->gridButtons->verticalSpacing())) + (this->contentsMargins().top()+this->contentsMargins().bottom())
	);
	this->setGeometry(QStyle::alignedRect(
		Qt::LeftToRight,
		Qt::AlignCenter,
		windowsize,
		this->screen()->availableGeometry()
	));
}
void Soundboard::clearInterface()
{
	for(HotkeyButton *hkb : this->lHotkeyButtons)
		hkb->deleteLater();
	this->lHotkeyButtons.clear();
}

void Soundboard::loadSettings()
{
	this->bEnabled = this->settingsGlobal.value("Enabled", true).toBool();
	this->bRunOnStartup = this->settingsGlobal.value("RunOnStartup", false).toBool();
	this->bStartMinimised = this->settingsGlobal.value("StartMinimised", false).toBool();
	this->bMinimiseToTaskbar = this->settingsGlobal.value("MinimiseToTaskbar", false).toBool();
	this->bCloseToTaskbar = this->settingsGlobal.value("CloseToTaskbar", false).toBool();
}
void Soundboard::loadMoreSettings()
{
	this->sizeButtonLayout.setWidth(this->settingsGlobal.value("ButtonColumns", this->sizeButtonLayout.width()).toUInt());
	this->sizeButtonLayout.setHeight(this->settingsGlobal.value("ButtonRows", this->sizeButtonLayout.height()).toUInt());
	this->iButtonSize = this->settingsGlobal.value("ButtonSize", this->iButtonSize).toInt();

	QString shortcut = this->settingsGlobal.value("EnableHotkey","").toString();
	if(!shortcut.isEmpty())	this->hkEnable->setShortcut(QKeySequence(shortcut), true);
	else										this->hkEnable->setRegistered(false);

	shortcut = this->settingsGlobal.value("ReconnectHotkey","").toString();
	if(!shortcut.isEmpty())	this->hkReconnect->setShortcut(QKeySequence(shortcut), true);
	else										this->hkReconnect->setRegistered(false);
}

void Soundboard::saveSettings()
{
#ifdef Q_OS_WINDOWS
	this->bRunOnStartup = this->actRunOnStartup->isChecked();
	this->settingsGlobal.setValue("RunOnStartup", this->bRunOnStartup);
#endif
	this->bEnabled = this->actEnabled->isChecked();
	this->bStartMinimised = this->actStartMinimised->isChecked();
	this->bMinimiseToTaskbar = this->actMinimiseToTaskbar->isChecked();
	this->bCloseToTaskbar = this->actCloseToTaskbar->isChecked();

	this->settingsGlobal.setValue("Enabled", this->bEnabled);
	this->settingsGlobal.setValue("StartMinimised", this->bStartMinimised);
	this->settingsGlobal.setValue("MinimiseToTaskbar", this->bMinimiseToTaskbar);
	this->settingsGlobal.setValue("CloseToTaskbar", this->bCloseToTaskbar);

	this->settingsGlobal.setValue("ButtonColumns", this->sizeButtonLayout.width());
	this->settingsGlobal.setValue("ButtonRows", this->sizeButtonLayout.height());
}

void Soundboard::setEnabled(bool e)
{
	this->saveSettings();

	if(e)
		this->enableAllHotkeys();
	else
		this->disableAllHotkeys();
}
void Soundboard::toggleEnabled()
{
	this->actEnabled->toggle();
}

void Soundboard::setRunOnStartup(bool r)
{
	const QString applicationname = QCoreApplication::applicationName();
	const QString applicationpath = QDir::toNativeSeparators(QCoreApplication::applicationFilePath());
#ifdef Q_OS_WINDOWS
	QSettings settings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
#endif
	if(r) {
		settings.setValue(applicationname, applicationpath);
	} else {
		settings.remove(applicationname);
	}
	this->saveSettings();
}

void Soundboard::enableAllHotkeys()
{
	for(HotkeyButton *hkb : this->lHotkeyButtons)
		hkb->setGlobalEnabled(this->bEnabled);
}
void Soundboard::disableAllHotkeys()
{
	for(HotkeyButton *hkb : this->lHotkeyButtons)
		hkb->setGlobalEnabled(false);
}

void Soundboard::reloadAllAudioDevices()
{
	RtAudio::DeviceInfo device = Soundboard::findSelectedAudioDevice();
	for(HotkeyButton *hkb : this->lHotkeyButtons)
		hkb->setNewAudioDevice(device);
}

void Soundboard::setAllVolumes(qreal db)
{
	for(HotkeyButton *hkb : this->lHotkeyButtons)
		hkb->changeVolume(db);
}

RtAudio::DeviceInfo Soundboard::findSelectedAudioDevice()
{
	QSettings settings;
	const QString devicenamesaved = settings.value("OutputDevice", "").toString();
	RtAudio *rtaudio = new RtAudio(RtAudio::WINDOWS_WASAPI);
	const quint32 devicecount = rtaudio->getDeviceCount();
	for(quint32 i=0; i<devicecount; i++) {
		RtAudio::DeviceInfo info = rtaudio->getDeviceInfo(i);
		if((info.outputChannels > 0) && devicenamesaved == QString(info.name.c_str()))
			return info;
	}
	return rtaudio->getDeviceInfo(rtaudio->getDefaultOutputDevice());

//	QList<QAudioDeviceInfo> devices = QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
//	QSettings settings;
//	const QString devicenamesaved = settings.value("OutputDevice", "").toString();
//	for(const QAudioDeviceInfo &device : devices)
//		if(device.deviceName() == devicenamesaved)
//			return device;
//	return QAudioDeviceInfo::defaultOutputDevice();
}

void Soundboard::changeEvent(QEvent *e)
{
	if(e->type() == QEvent::WindowStateChange && this->isMinimized() && this->bMinimiseToTaskbar)
		this->hide();
	else
		QMainWindow::changeEvent(e);
}

void Soundboard::closeEvent(QCloseEvent *e)
{
	if(this->bCloseToTaskbar) {
		this->hide();
		e->ignore();
	} else {
		this->stiTrayIcon->hide();
		QMainWindow::closeEvent(e);
	}
}

void Soundboard::showMoreSettings()
{
	if(this->msMoreSettingsPanel) this->msMoreSettingsPanel->deleteLater();
	this->msMoreSettingsPanel = new MoreSettings(this->sizeButtonLayout, this->iButtonSize, this);
	QObject::connect(this->msMoreSettingsPanel, &MoreSettings::audioDeviceUpdated, this, &Soundboard::reloadAllAudioDevices);
	QObject::connect(this->msMoreSettingsPanel, &MoreSettings::audioVolumeUpdated, this, &Soundboard::setAllVolumes);
	QObject::connect(this->msMoreSettingsPanel, &MoreSettings::closedMoreSettings, this, &Soundboard::applyMoreSettings);
	this->msMoreSettingsPanel->show();
}
void Soundboard::applyMoreSettings()
{
	QSize oldlayout = this->sizeButtonLayout;
	quint8 oldbuttonsize = this->iButtonSize;
	this->loadMoreSettings();
	if((oldlayout.width() != this->sizeButtonLayout.width()) || (oldlayout.height() != this->sizeButtonLayout.height())) {
		this->clearInterface();
		this->buildInterface();
	}
	if(oldbuttonsize != this->iButtonSize)
	{
		this->resizeWindow();
	}

	this->msMoreSettingsPanel->hide();
}

void Soundboard::showFromTray(QSystemTrayIcon::ActivationReason reason)
{
	if(reason==QSystemTrayIcon::Trigger) {
		this->showNormal();
		this->activateWindow();
	}
}

void Soundboard::exitApplication()
{
	this->stiTrayIcon->hide();
	QApplication::quit();
}
