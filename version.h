#ifndef VERSION_H
#define VERSION_H
namespace Version
{
    static const int MAJOR = 0;
    static const int MINOR = 7;
    static const int REVISION = 26;
    static const int BUILD = 461;
}
#endif // VERSION_H
