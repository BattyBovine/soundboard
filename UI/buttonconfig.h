#ifndef BUTTONCONFIG_H
#define BUTTONCONFIG_H

#include <QDialog>
#include <QFileInfo>
#include <QSettings>

namespace Ui {
class ButtonConfig;
}

class ButtonConfig : public QDialog
{
	Q_OBJECT

public:
	explicit ButtonConfig(QPoint, QWidget *parent = nullptr);
	~ButtonConfig();

	bool enabled() { return this->bEnabled; }
	bool startStop() { return this->bStartStop; }
	QKeySequence inputKey() { return this->ksShortcut; }
	QFileInfo soundFile() { return fiSound; }
	bool loop() { return this->bStartStop ? this->bLoop : false; }

	static QString buttonConfigCategory(QPoint p) { return QString("Buttons/Column%1/Row%2/").arg(p.x()).arg(p.y()); }

public slots:
	void enableHotkey(bool);
	void setStartStop(bool);
	void setKeySequence(QKeySequence);
	void setSoundFile(QFileInfo);
	void loadSoundFile();
	void setLoop(bool);

private:
	Ui::ButtonConfig *ui;

	QSettings settings;
	QPoint pPosition;

	quint8 bEnabled : 1;
	quint8 bStartStop : 1;
	QKeySequence ksShortcut;
	QFileInfo fiSound;
	quint8 bLoop : 1;
};

#endif // BUTTONCONFIG_H
