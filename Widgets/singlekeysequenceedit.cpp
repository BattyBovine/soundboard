#include "singlekeysequenceedit.h"

void SingleKeySequenceEdit::keyPressEvent(QKeyEvent *e)
{
	QKeySequenceEdit::keyPressEvent(e);
	this->setKeySequence(this->keySequence()[0]);
	emit(this->keySequenceChanged(this->keySequence()));
	emit(this->editingFinished());
}
