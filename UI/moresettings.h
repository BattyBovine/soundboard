#ifndef MORESETTINGS_H
#define MORESETTINGS_H

#include <QDialog>
#include <QCloseEvent>
#include <QSettings>

namespace Ui {
class MoreSettings;
}

class MoreSettings : public QDialog
{
	Q_OBJECT

public:
	explicit MoreSettings(QSize, quint8, QWidget *parent = nullptr);
	~MoreSettings();

protected:
	virtual void closeEvent(QCloseEvent*) override;

signals:
	void audioDeviceUpdated(QString);
	void audioVolumeUpdated(qreal);
	void closedMoreSettings();

private slots:
	void changeColumnsValue(quint8);
	void changeRowsValue(quint8);
	void changeButtonSizeValue(quint8);
	void changeEnableToggleShortcut(QKeySequence);
	void changeReconnectShortcut(QKeySequence);

	void changeOutputDevice(QString);
	void changeVolume(qint16);

private:
	void setVolumeText(qreal);

	Ui::MoreSettings *ui;

	QSettings settingsGlobal;
};

#endif // MORESETTINGS_H
