#include "UI/soundboard.h"

#include <QApplication>
#include <QSharedMemory>
#include <QSettings>

int main(int argc, char *argv[])
{
	QSharedMemory singleinstance("549d017a-4e37-427b-be82-8195ecbbcd7f");
	if(!singleinstance.create(512,QSharedMemory::ReadWrite))
		return 0;

	QCoreApplication::setOrganizationName("Batty Bovine Productions, LLC");
	QCoreApplication::setOrganizationDomain("battybovine.com");
	QCoreApplication::setApplicationName("Soundboard");

	QApplication a(argc, argv);
	Soundboard w;
	return a.exec();
}
